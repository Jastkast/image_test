<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageGetRequest;
use App\Http\Requests\ImageStoreRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Imagick;

class ImageController extends Controller
{
    public function store(ImageStoreRequest $request)
    {
        $request->image->storeAs(
            'images',
            $request->filename . '.' . $request->image->extension()
        );

        return response()->json([
            'success' => true,
            'statusCode' => 200
        ]);
    }

    public function get($filters, $filename, ImageGetRequest $request)
    {
        $parameters = $this->parseParameters($filters);

        try {
            $imagick = new Imagick(storage_path('app/images/' . $filename));
        } catch (\Exception $e) {
            $this->throwExResponse('There is no such file.');
        }

        if ($parameters !== 'origin') {
            try {
                $imagick->thumbnailImage(
                    $parameters['w'] ?? $imagick->getImageWidth(),
                    $parameters['h'] ?? $imagick->getImageHeight()
                );
            } catch (\Exception $e) {
                $this->throwExResponse('Width or height exceeds limit.');
            }
            $imagick->setImageCompressionQuality($parameters['q'] ?? 100);
        }

        return response(
            $imagick->getImageBlob(),
            200,
            [
                'Content-type' => 'image/jpg',
                'Content-Disposition' => 'attachment; filename="' . $filename . '"',
            ]
        );
    }

    private function parseParameters($filters)
    {
        preg_match_all(
            '/(q|h|w)_\d+|^origin$/i',
            $filters,
            $matches
        );

        $matches = collect($matches[0]);

        if ($matches->values()->first() === 'origin') {
            return 'origin';
        }

        return $matches->mapWithKeys(function ($val) {
            $exploded = explode('_', $val);
            return [
                $exploded[0] => $exploded[1]
            ];
        });
    }

    private function throwExResponse($message){
        throw new HttpResponseException(response()->json([
            'success' => false,
            'statusCode' => 500,
            'error' => $message
        ]));
    }
}